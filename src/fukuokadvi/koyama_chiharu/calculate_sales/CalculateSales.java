package fukuokadvi.koyama_chiharu.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> branchmap = new HashMap<>();
		Map<String, Long> salesmap = new HashMap<>();
		BufferedReader br = null;

		try {
			//エラーメッセージ①
			//支店定義ファイル読み込み・保持
			File file= new File(args[0],"branch.lst");
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String code;
			while((code= br.readLine()) != null) {

				//分割
				String[] deta = code.split(",");

				//エラー表示②
				if (!deta[0].matches("\\d{3}") || deta.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				//データ取り込み
				branchmap.put(deta[0], deta[1]);

				//売上ファイル合算用
				salesmap.put(deta[0],0L);
			}
			//フィルタリング
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str){
					if (str.matches("\\d{8}.rcd")){
						return true;
					}else{
						return false;
					}
				}
			};
			File[] salesfile = new File(args[0]).listFiles(filter);

			//エラー表示③
			//昇順で並び替え
			Arrays.sort(salesfile,Comparator.naturalOrder());
			//最小値
			String number = salesfile[0].getName();
			String[] numbers = number.split("\\.");
			int min = Integer.parseInt(numbers[0]);
			//最大値
			String num = salesfile [salesfile.length-1].getName();
			String[] nums = num.split("\\.");
			int max = Integer.parseInt(nums[0]);
			//増えるはずの数
			int size = salesfile.length -1;
			if(size+min!=max) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			//売上読み込み
			BufferedReader br1 = null;
			for(File file8 : salesfile){
				try {
					br1 = new BufferedReader(new FileReader(file8));

					//一行ずつ読み込み
					String branchcode = br1.readLine();
					String sales = br1.readLine();
					if(sales == null || !sales.matches("[0-9]+")) {
						System.out.println(file8.getName()+"のフォーマットが不正です");
						return ;
					}
					//エラー表示⑥
					if( br1.readLine() != null) {
						System.out.println(file8.getName()+"のフォーマットが不正です");
						return ;
					}
					//エラー表示⑤

					if (!branchmap.containsKey(branchcode)){
						System.out.println(file8.getName()+"の支店コードが不正です");
						return;
					}
					//String型からlong型に変換
					Long sale = Long.parseLong(sales);

					//売上加算
					salesmap.put(branchcode,salesmap.get(branchcode)+sale);

					//エラー表示④
					if(!salesmap.get(branchcode).toString().matches("^\\d{1,10}$")){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}finally {
					if(br1 != null) {
						try {
							br1.close();
						}
						catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
				}
			}

			//ファイル出力
			File files = new File(args[0], "branch.out");
			BufferedWriter bw;
			try {
				bw = new BufferedWriter(new FileWriter(files));
				for(String key : salesmap.keySet()){
					bw.write(key + ","  + branchmap.get(key)  + "," + salesmap.get(key));
					bw.newLine();
				}
				bw.close();
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
	}
}



